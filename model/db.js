const colors = require("colors");
const mongo = require("mongodb");
var db = null;
// Соединение Монго


var EXP = {
	get: () => db,
	info: { history: [], users: [], rooms: [], private: [] },
	connect: http => {
		mongo.connect("mongodb://localhost:27017/chat", (err, database) => {
			if (err) throw err;

			db = database;

			http.listen(7879, () => {
				console.log(`----Server started----`.rainbow)
			});
		});
		return http;
	}
}

module.exports = EXP;