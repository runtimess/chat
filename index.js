const express = require("express");
const app = express();
const http = require("http").Server(app);
const db = require("./model/db"); 
const io = require("socket.io")(db.connect(http)); 
const bodyParser = require("body-parser");
const routes = require("./controller/routes");
require("./controller/sockets").IOInit(io); // IO Connection

app.set("view engine", "ejs");

// Промежуточные обработчики
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname));


app.get("/", routes.index)
app.get("/create", routes.createRoom);
app.get("/room/:id", routes.room);
app.get("/create/:reply", routes.createPrivate);
app.get("/private/:id", routes.private);
